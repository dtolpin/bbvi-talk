# Black Box Variational Inference slides and Anglican worksheet

The worksheet is a motivation of probabilistic programming, 
structured probabilistic models, and variation inference.
Consult [anglican-user](https://bitbucket.org/probprog/anglican-user) for instructions on how to play with it.


The slides are along the lines of papers on Black Box
Variational Inference in general, and in probabilistic
programming in particular.
