\documentclass{beamer}

\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[ruled]{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{tikz}

\newcommand {\I} {\ensuremath {\mathbf{1\hspace{-5.5pt}1}}}
\renewcommand{\vec}[1]{\ensuremath{\boldsymbol{#1}}}

\title{Variational Inference in Probabilistic Programs}
\date{March 31th, 2016}
  
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\begin{frame}
\titlepage

\centering
	Slides and worksheet: \url{http://bitbucket.org/dtolpin/bbvi-talk/}

	Built on slides prepared by Brooks Paige.
	
	Based on: \url{http://arxiv.org/abs/1301.1299},
		\url{http://www.cs.columbia.edu/~blei/papers/RanganathGerrishBlei2014.pdf}
		and 
		\url{http://arxiv.org/abs/1507.04635}
		 
\end{frame}

\begin{frame}{Variation inference: basic idea}
	Consider a structured probabilistic model: 
	\begin{itemize}
		\item $\mathbf{x}$ --- latent random variables,
		\item $\mathbf{y}$ --- observations. 
	\end{itemize}
	We want to know how $\mathbf{x}$ is distributed. Formally:
	\begin{itemize}
		\item Given: joint distribution $p(\mathbf{x},\mathbf{y})$, generally as a likelihood and prior.
			$$ p(\mathbf{x}, \mathbf{y}) = p(\mathbf{y}|\mathbf{x})p(\mathbf{x}) $$
		\item Desired: posterior distribution $p(\mathbf{x}|\mathbf{y})$, generally intractable.
		\item \textbf{Variational inference:} approximate $p(\mathbf{x},\mathbf{y})$ by a parametric distribution $q(\mathbf{x}|\lambda)$, and \textit{optimize} $\lambda$.
	\end{itemize}

	Optimization goal: $\min D_{KL}(p(\mathbf{x}, \mathbf{y})||q(\mathbf{x}|\lambda))$
\end{frame}

\begin{frame}{ELBO --- Evidence Lower BOund}

	KL divergence contains an unknown quantity $p(\mathbf{x}|\mathbf{y})$. 

	However, if we decompose $\log p(\mathbf{y})$ (which is a constant):

	\begin{align*}
		\log p(\mathbf{y}) =& \int q(\mathbf{x}|\lambda) \log \left[\frac {p(\mathbf{x},\mathbf{y})} {q(\mathbf{x}|\lambda)} \right] dx +  \int q(\mathbf{x}|\lambda) \log \left[\frac {q(\mathbf{x}|\lambda)} {p(\mathbf{x}|\mathbf{y})} \right] dx \\ 
		=& \mathcal{L}(\lambda)+D_{KL}(q||p)
	\end{align*}	
	
	\begin{itemize}
		\item To minimize $D_{KL}(q||p)$ we need to maximize $\mathcal{L}(\lambda)$.
		\item If $p(\mathbf{x}, \mathbf{y})$ has special form (conjugate-exponential), we can derive closed form updates.
		\item In most cases, there is no closed-form solution, and we need \textit{Black-Box} variational inference.
	\end{itemize}
\end{frame}

\begin{frame}{Stochastic Gradient Ascent}
	Theoretically, we can use stochastic gradient descent:
	$$ \nabla_\lambda \mathcal{L}(\lambda)= \mathbb{E}_q\left[ \log \left[ \frac {p(\mathbf{x}, \mathbf{y})} {q(\mathbf{x}|\lambda)} \right] \nabla_\lambda \log q(\mathbf{x}|\lambda) \right]$$
    For that we need to know:
	\begin{itemize}
		\item How to evaluate $q(\mathbf{x}|\lambda)$ and $\nabla_\lambda q(\mathbf{x}|\lambda)$ --- defined once per distribution type, independently of the model.
		\item How to approximate expectations over $q$ --- 
			\pause
			\begin{itemize} 
				\item Monte-Carlo sampling would be too noisy ...
					\pause
				\item but there are some tricks that help.
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Stochastic Gradient Ascent - cont.}
	With $\mathbf{x}^s \sim q(\mathbf{x}|\lambda)$, we have
	$$ \nabla_\lambda \mathcal{L}(\lambda)= \frac 1 S \sum _{s=1}^S \log \left[ \frac {p(\mathbf{x}^s, \mathbf{y})} {q(\mathbf{x}^s|\lambda)} \right] \nabla_\lambda \log q(\mathbf{x}^s|\lambda)$$
	\begin{itemize}
		\item Works for any $p(\mathbf{y}|\mathbf{x})$ and $p(\mathbf{x})$ (specification of the model).
		\item Works for any differentiable $q$.
		\item Normally, $q$ is \textit{factorized}:
			$$q(\textbf{x}|\lambda) = \prod_{i=1}^M
	q_i(\textbf{x}|\lambda)\,,$$ where $q_i$ are library distributions. This
	corresponds to the \textit{mean field theory} in physics and called mean
	field assumption.  
	\end{itemize}
\end{frame}

\begin{frame}{Algorithm}
	\includegraphics[scale=1.3]{bbvi-algorithm.pdf}
\end{frame}

\begin{frame}{Probabilistic Program}
	\textbf{Probabilistic program $\mathcal{P}$:}
			\begin{itemize}
				\item A program with random computations $\mathbf{x}$.
				\item Distributions are conditioned by `observations' $\mathbf{y}$.
				\item Values of certain expressions are `predicted' --- \textbf{the output}.
			\end{itemize}

			Can be written in any language (extended by \texttt{sample} and \texttt{observe}).
	The probability of a \textbf{program trace} is 
	$$p(\mathbf{x},\mathbf{y})=\prod_{i=1}^{\left|\pmb{x}\right|} p_i(x_i|x_{<i}) \prod_{j=1}^{\left|\pmb{y}\right|}p_j(y_{j}|x_{\le j})$$.

\end{frame}

\begin{frame}{Mean Field Probabilistic Program}
	\begin{itemize}
		\item For each \texttt{(sample $p_i(x_i|x_{<i})$)} checkpoint, instead of sampling $x_i$ from $p$, sample from variational $q_i(x_i|\lambda)$.
		\item Compute probability $\log q(\mathbf{x}|\lambda)$.
		\item Compute gradient $\nabla_\lambda \log q(\mathbf{x}|\lambda)$.
		\item Compute trace probability $\log p(\mathbf{x}, \mathbf{y})$.
	\end{itemize}
	The approximating program has no \texttt{observe} statements! 
	\begin{itemize}
		\item We only need to define $\nabla_\lambda q(\mathbf{x}|\lambda)$ for library distributions (Normal, Gamma, Beta, ...), once and for all times.
	\end{itemize}
\end{frame}


\begin{frame}{Variational Inference in Probabilistic Programs}
	\includegraphics[scale=1.0]{bbvi-pprog.pdf}
\end{frame}

\begin{frame}{Uses and Applications}

	Other black-box inference methods:

	\begin{itemize}
		\item Markov Chain Monte Carlo methods (Metropolis-in-Gibbs).
		\item Particle Filtering (in particular, state-space models).
		\item Combination of particle filtering and MCMC.
	\end{itemize}

	When to use variational inference (for faster inference or 
	unique functionality):

	\begin{itemize}
		\item The model seems to be factorizable.
		\item We are interested in the 'mean field', that is,
			high probability behavior.
		\item Policy learning (Jan Willem van de Meent et al, ..., 2015):
			\url{http://www.robots.ox.ac.uk/~jwvdm/talks/bbps-pps/assets/player/KeynoteDHTMLPlayer.html}
	\end{itemize}
\end{frame}

\begin{frame}
    \begin{center}
        \begin{huge}
            Thank You
        \end{huge}
    \end{center}
\end{frame}

\end{document}
